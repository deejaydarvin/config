{
  description = "Configuration for Aragorn (MacBook Pro 2024)";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    darwin.url = "github:lnl7/nix-darwin";
    darwin.inputs.nixpkgs.follows = "nixpkgs";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    # not sure if this deepsec stuff works ... I could not fine the binary...
    deepsec.url = "github:rkunnema/deepsec";
    deepsec.follows = "nixpkgs";

    nixvim = {
        url = "github:nix-community/nixvim";
        # If using a stable channel you can use `url = "github:nix-community/nixvim/nixos-<version>"`
        inputs.nixpkgs.follows = "nixpkgs";
    };
    # nixvim.url = "github:nix-community/nixvim";
    #   # If you are not running an unstable channel of nixpkgs, select the corresponding branch of nixvim.
    #   # url = "github:nix-community/nixvim/nixos-24.11";
    # nixvim.follows = "nixpkgs";
  };

  outputs = inputs@{ nixpkgs, home-manager, darwin, deepsec, nixvim,... }:
   {
      # overlays = {
      # Overlays to add various packages into package set
      # deepsec = final: prev: {
      #   deepsec = import inputs.deepsec { inherit (prev) pkgs; };
      # };

      # Overlay useful on Macs with Apple Silicon
      # apple-silicon = final: prev: optionalAttrs (prev.stdenv.system == "aarch64-darwin") {
      #     # Add access to x86 packages system is running Apple Silicon
      #     pkgs-x86 = import inputs.nixpkgs-unstable {
      #       system = "x86_64-darwin";
      #       inherit (nixpkgsConfig) config;
      #     };
      #   };
      # };

    # Build darwin flake using:
    # $ darwin-rebuild build --flake .#simple
    darwinConfigurations = {
      "aragorn" = darwin.lib.darwinSystem {
        system = "aarch64-darwin";
        modules = [
          ./configuration.nix

          home-manager.darwinModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.robert = import ./home.nix;
            home-manager.backupFileExtension = "nix-backup";
            # Optionally, use home-manager.extraSpecialArgs to pass
            # arguments to home.nix
            home-manager.extraSpecialArgs = { inherit deepsec nixvim; };
            home-manager.sharedModules = [
               # nixvim.nixDarwinModules.nixvim 
               nixvim.homeManagerModules.nixvim 
            ];

          }

        ];

      };
    };
  };
}
