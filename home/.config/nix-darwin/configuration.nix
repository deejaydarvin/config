{config, pkgs, self, home-manager,  ... }:
{
      # List packages installed in system profile. To search by name, run:
      # $ nix-env -qaP | grep wget
      environment.systemPackages = with pkgs;
        [ vim
          homesick
          pass

          fzf
          fasd
          silver-searcher
          pure-prompt
          fortune-kind

          git-annex
          git-lfs

          tamarin-prover
          # pkgs.texlive.withPackages (pkgs: with pkgs; [
          #   texdoc # recommended package to navigate the documentation
          # ])
          # texlive.combine {
          #   inherit (texlive) scheme-small collection-langkorean algorithms cm-super;
          # }
          texlive.combined.scheme-full
          texlivePackages.tex-gyre
          texlivePackages.tex-gyre-math

          typst
        ];

        fonts.packages = with pkgs; [
          gyre-fonts
        ];

      # Necessary for using flakes on this system.
      nix.settings.experimental-features = "nix-command flakes";

      # Enable alternative shell support in nix-darwin.
      # programs.fish.enable = true;

      # Set Git commit hash for darwin-version.
      #system.configurationRevision = self.rev or self.dirtyRev or null;

      # Used for backwards compatibility, please read the changelog before changing.
      # $ darwin-rebuild changelog
      system.stateVersion = 5;

      # The platform the configuration will be used on.
      nixpkgs.hostPlatform = "aarch64-darwin";

      system.keyboard = {
        enableKeyMapping = true;
        remapCapsLockToEscape = true;
      };

      system.defaults = {
        dock = {
            mru-spaces = false;
            orientation = "left";
            wvous-bl-corner = 4; # hot corner (bottom left): show desktop 
            wvous-br-corner = 3; # application windows
            wvous-tr-corner = 2; # mission control
          };

        controlcenter.Bluetooth = true; # show bluetooth icon

        finder.FXPreferredViewStyle = "clmv";
        screencapture.location = "~/Pictures";
        NSGlobalDomain."com.apple.swipescrolldirection" = false;
      };

      programs.gnupg.agent = {
          enable = true;
          enableSSHSupport = true;
      };

      programs.man.enable = true;

      programs.zsh = {
        enableCompletion = true;
        enableFastSyntaxHighlighting = true;
        enableFzfCompletion = true;
        enableFzfHistory = true;
      };

      programs.vim = {
        enable = true;
        enableSensible = true;
      };

      homebrew = {
        enable = true;
        brews = [
            "wireguard-go"
          ];
        masApps = {
            Wireguard = 1451685025;
          };
        casks = [
            #essential
            "firefox"
            "rectangle"
            "signal"
            "iterm2"

            # work
            "zoom"
            "nextcloud"
            "element"

            # academic work
            "zotero"
            "drawio"
            "skim"
            "isabelle"

            # productivity
            "logseq"
            "macdown"

            # fun
            "youtube-downloader"
            "steam"
            "vlc"
            "wine-stable"
            "spotify"
            # "gog-galaxy"
          ];
        onActivation = {
          autoUpdate = true;
          cleanup = "uninstall";
          upgrade = true;
        };
      };

      users.users.robert = {
        name = "robert";
        home = "/Users/robert";
          };
}
