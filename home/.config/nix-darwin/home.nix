{config, pkgs, lib, deepsec, inputs, nixvim, ... }:

let
  fromGitHub = rev: ref: repo: pkgs.vimUtils.buildVimPlugin {
    pname = "${lib.strings.sanitizeDerivationName repo}";
    version = ref;
    src = builtins.fetchGit {
      url = "https://github.com/${repo}.git";
      ref = ref;
      rev = rev;
    };
  };

  # nixvim = import (builtins.fetchGit {
  #   url = "https://github.com/nix-community/nixvim";
  #       # When using a different channel you can use `ref = "nixos-<version>"` to set it here
  #   # rev 
  #     });

  system = "aarch64-darwin";
in
{
  # The state version is required and should stay at the version you
  # originally installed.
  home.stateVersion = "24.11";
  # Htop
  # https://rycee.gitlab.io/home-manager/options.html#opt-programs.htop.enable
  #
  # imports = [
    # nixvim.homeManagerModules.nixvim
  # ];

  
  programs.htop.enable = true;
  programs.htop.settings.show_program_path = true;

  home.packages = (with pkgs ;  [
    # Some basics
    coreutils
    curl
    wget

    # Dev stuff
    haskellPackages.cabal-install
    haskellPackages.hoogle
    haskellPackages.hpack
    haskellPackages.implicit-hie
    haskellPackages.stack

    # security stuff
    # tex stuff
    tectonic
    texlab

  ] ++ lib.optionals stdenv.isDarwin [
    m-cli # useful macOS CLI commands
  ])  ++
  # packages outside pkgs.*
  [
    # deepsec.defaultPackage.${system}
    # deepsec.packages.${system}.default
    # deepsec.defaultPackage.${system}
    # deepsec.packages.default
    deepsec
    # deepsec.defaultPackage
  ]
  ;

  programs.fzf = {
    enable = true;
    enableZshIntegration = true;
  };

  programs.zsh = {
	enable = true;
	enableCompletion = true;
	syntaxHighlighting.enable = true;
  enableVteIntegration = true;
  # autosuggestion.enable = true;
  defaultKeymap = "viins";
  history.size = 10000;
  history.save = 10000;

  # prezto.prompt.theme = "pure";
  # prezto.tmux.itermIntegration = true;

  initExtra = ''
#open command line in vim using Esc-v
export VISUAL=vim
autoload edit-command-line; zle -N edit-command-line
bindkey -M vicmd v edit-command-line

# navigate in autocompletion menu
zstyle ':completion:*' menu select

# quick jump to past directories
DIRSTACKSIZE=8
setopt autopushd pushdminus pushdsilent pushdtohome

autoload -U promptinit; promptinit
prompt pure

    eval "$(fasd --init auto)"

    function mkword (){
    pandoc $1 --reference-doc=$HOME/.pandoc/reference-files/cispaletter.docx -t docx -o \$\{1:r\}.docx
    }
        '';
  shellAliases = {
          "ls"="ls --color";
          "info"="info --vi-keys";
          "grep"="grep --color=auto";
          "bc"="bc -l";
          "pisync"="ssh pi \\~/bin/gitsync.sh";
          "pirw"="ssh pi \\~/bin/mount_rw.sh";
          "piro"="ssh pi \\~/bin/mount_ro.sh";
          "mpisc"="ncmpcpp -c ~/.ncmpcpp/piscine";
          "mext-pisc"="ncmpcpp -c ~/.ncmpcpp/piscine-ext";
          "speedtest"="curl -o /dev/null http://speedtest.wdc01.softlayer.com/downloads/test100.zip";
    };
  };

  home.sessionVariables = {
          # not the right directory...
            # DEEPSEC_DIR = "${deepsec}/bin";
  };

  # programs.lsd = {
  #     enable = true;
  #     enableAliases = true;
  #   };

  programs.dircolors = {
    enable  = true;
    enableZshIntegration = true;
  }; 

  programs.vim = {
    enable = true;
    packageConfigurable = pkgs.vim-darwin;
    plugins = with pkgs.vimPlugins ; [
      # included in sensible
      fugitive # make git nice
      vim-surround # cs( to change things surrounded by ()
      ctrlp
      tabular
      # vim-sensible
      vim-commentary # gcc for commenting
      # vim-dispatch
      vim-unimpaired #  ]a to jump between arguments etc.
      # vim-eunuch
      vim-matchup # use % to jump between syntactic elements
      vim-repeat # repeat macros with .
      vim-vinegar # make netrw bearable
      ultisnips
      vim-snippets
      vimtex 
      jellybeans-vim
 # "ag"
      todo-txt-vim
      vim-LanguageTool
      (fromGitHub "758966ca920c17fc227b7b05a6225e8c5914b7ca" "master" "tamarin-prover/editors")
      (fromGitHub "5ae230d8281369b93732992dbe2b36c97a2321f1" "master" "deejaydarvin/vim-spelllangcheck")
      # todo make nix flake for tamarin
       ];
       extraConfig = "colorscheme jellybeans"
        +
         (builtins.readFile ./vimrc) ;

     };

  programs.nixvim = {
      enable = true;
      # colorschemes.catppuccin.enable = true;
      # colorschemes.monokai-pro.enable = true;
      colorschemes.vscode.enable = true;
      plugins = {
        commentary.enable = true;
        vim-matchup.enable = true;
        repeat.enable = true;
        cmp = {
          autoEnableSources = true;
          settings.sources = [
            { name = "nvim_ultisnips"; }
            { name = "latex-symbols"; }
            { name = "path"; }
            { name = "buffer"; }
          ];
        };
        friendly-snippets.enable = true;
        todo-comments.enable = true;
        lsp = {
          enable = true;
          inlayHints = true;
          servers.tinymist = {
            enable = true;
            settings = {
              formatterMode = "typstyle";
            };
          };
        };
        luasnip.enable = true;
        # tinymist.enable = true;
      };
      extraPlugins = with pkgs.vimPlugins ; [
      ctrlp
      tabular
      vim-unimpaired #  ]a to jump between arguments etc.
      vim-vinegar # make netrw bearable
      # ultisnips
      # vim-snippets
      # vimtex 
 # "ag"
      todo-txt-vim
      vim-LanguageTool
      (fromGitHub "758966ca920c17fc227b7b05a6225e8c5914b7ca" "master" "tamarin-prover/editors")
      (fromGitHub "5ae230d8281369b93732992dbe2b36c97a2321f1" "master" "deejaydarvin/vim-spelllangcheck")
      # nvim specific
      typst-preview-nvim
       ];
      clipboard.register = "unnamedplus";
      extraConfigVim = (builtins.readFile ./vimrc) ;
  };

  programs.git = {
    enable = true;
    userName = "Robert Künnemann";
    userEmail = "robert.kuennemann@cispa.de";
  };

 programs.direnv = {
      enable = true;
      enableZshIntegration = true; # 
      nix-direnv.enable = true;
    };

programs.zed-editor = {
        enable = true;
        extensions = ["nix" "toml" "elixir" "make"];
        userKeymaps = [
          { context = "EmptyPane || SharedScreen || vim_operator == none && !VimWaiting && vim_mode != insert";
            bindings = {
                  # ctrl-shift-t = "workspace::NewTerminal";
                  "\\ b"= "tab_switcher::Toggle";
                  "\\ f"= "file_finder::Toggle";
                  "\\ m"= "task::Rerun";
                }; 
          }
        ];

        ## everything inside of these brackets are Zed options.
        userSettings = {

          assistant = {
            enabled = true;
            version = "2";
            default_open_ai_model = null;
                ### PROVIDER OPTIONS
                ### zed.dev models { claude-3-5-sonnet-latest } requires github connected
                ### anthropic models { claude-3-5-sonnet-latest claude-3-haiku-latest claude-3-opus-latest  } requires API_KEY
                ### copilot_chat models { gpt-4o gpt-4 gpt-3.5-turbo o1-preview } requires github connected
                default_model = { 
                  provider = "zed.dev";
                  model = "claude-3-5-sonnet-latest";
                };

                #                inline_alternatives = [
                #                    {
                #                        provider = "copilot_chat";
                #                        model = "gpt-3.5-turbo";
                #                    }
                #                ];
              };

              hour_format = "hour24";
              auto_update = false;
              terminal = {
                alternate_scroll = "off";
                blinking = "off";
                copy_on_select = false;
                dock = "bottom";
                detect_venv = {
                  on = {
                    directories = [".env" "env" ".venv" "venv"];
                    activate_script = "default";
                  };
                };
                env = {
                  TERM = "alacritty";
                };
                font_family = "FiraCode Nerd Font";
                font_features = null;
                font_size = null;
                line_height = "comfortable";
                option_as_meta = false;
                button = false;
                shell = "system"; 
                toolbar = {
                  title = true;
                };
                working_directory = "current_project_directory";
              };

              lsp = {
                nix = { 
                  binary = { 
                    path_lookup = true; 
                  }; 
                };
                tinymist = {
                  initialization_options = {
                    "exportPdf" =  "onSave";
                    "outputPath"= "$root/$name";
                  };
                };
              };

            vim_mode = true;
            ## tell zed to use direnv and direnv can use a flake.nix enviroment.
            load_direnv = "shell_hook";
            base_keymap = "VSCode";
            theme = {
              mode = "system";
              light = "One Light";
              dark = "One Dark";
            };
            show_whitespaces = "all" ;
            ui_font_size = 14;
            buffer_font_size = 14;
        };
      };
  }
